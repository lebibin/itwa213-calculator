<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Welcome to CodeIgniter</title>
  <link rel="stylesheet" href="<?php echo asset_url() . 'css/bootstrap.css'; ?>">
</head>
<body>

<div id="container">


  <div id="body" class='text-center well span6 offset4' style='margin-top: 5em;'>
    <h1>Calculator</h1>
    <hr>
    <form class='form-inline' action="/calculator" method='post'>
      <input type="text" style='width: 25px;' placeholder='1'name='num1' value='<?php echo $num1; ?>'>
      <button class='btn btn-primary' type="submit" name='operation' value="+"><i class="icon-plus icon-white"></i></button>
      <button class='btn btn-primary' type="submit" name='operation' value="-"><i class="icon-minus icon-white"></i></button>
      <button class='btn btn-primary' type="submit" name='operation' value="*"><i class="icon-remove icon-white"></i></button>
      <button class='btn btn-primary' type="submit" name='operation' value="/"><span style='font-size: 19px; font-weight: bold;'>&#247;</span></button>
      <input type="text" style='width: 25px;' placeholder='9'name='num2' value='<?php echo $num2; ?>'>

    </form>
    <hr>
    <h4>
    <?php
      if($result != '')
        echo $result;
      else
        echo 'The result will go here.'
    ?>
    </h4>
</div>

</body>
</html>
